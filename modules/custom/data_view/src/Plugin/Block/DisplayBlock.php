<?php

/**
* Contains \Drupal\data_view\Plugin\Block\DisplayBlock
*/

namespace Drupal\data_view\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Display' Block.
 *
 * @Block(
 *   id = "display_block",
 *   admin_label = @Translation("Display Block"),
 *   category = @Translation("Display Data"),
 * )
 */
class DisplayBlock extends BlockBase
{
	/**
	* {@inheritdoc}
	*/
	public function build()
	{
		$query = \Drupal::database()->select('data_view_demo', 'd');
		$query->fields('d', ['id','vid','term_name','position']);
		$results = $query->execute()->fetchAll();
		$rows = array();
		foreach($results as $data){
			$rows[] = array(
				'id' =>$data->id,
				'vid' => $data->vid,
				'term_name' => $data->term_name,
				'position' => $data->position,
			);
		}
		return array(
			'#markup' => $this->t( 'Welcome in <b>'.$rows['id'].'</b>'),
		);
	}
}