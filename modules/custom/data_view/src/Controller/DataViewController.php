<?php

/**
 * Generates Drupal\data_view\Controller\DataViewController.
 */

namespace Drupal\data_view\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * 
 */
class DataViewController extends ControllerBase
{
	/**
	 * Generates a view page for data.
	 */
	public function display() {
		//create table header
		$header_of_table = array(
			'id'=>    t('Sr No'),
			'vid' => t('Taxonomy Term ID'),
			'term_name' => t('News Type'),
			'position' => t('Position'),
			'delete' => t('Operations'),
			// 'update' => t('Update'),
		);

		//select records from table
		$query = \Drupal::database()->select('data_view_demo', 'd');
		$query->fields('d', ['id','vid','term_name','position']);
		$results = $query->execute()->fetchAll();
		$rows=array();
		foreach($results as $data){
			$delete = Url::fromUserInput('/data_view/delete/'.$data->id);
			// $edit = Url::fromUserInput('/db_data/add?id='.$data->id);

			//print the data from table
			$rows[] = array(
				'id' =>$data->id,
				'vid' => $data->vid,
				'term_name' => $data->term_name,
				'position' => $data->position,
				
				\Drupal::l('Remove', $delete),
				// \Drupal::l('Edit', $edit),
			);
		}
		//display data in site
		$form['table'] = [
			'#type' => 'table',
			'#header' => $header_of_table,
			'#rows' => $rows,
			'#empty' => t('<b><i>No users found!</i></b>'),
		];
		return $form;
	}
}