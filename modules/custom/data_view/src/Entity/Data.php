<?php

/**
 * Generates Drupal\data_view\Entity\MyEntity.
 */

namespace Drupal\data_view\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityStorageInterface; 

/**
 * Defines the MyEntity class.
 * 
 * @ContentEntityType(
 *   id = "data_view_demo2",
 *   label = @Translation("Data View Demo"),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\data_view\Form\DataAddForm",
 *     }
 *   },
 *   links = {
 *     
 *   },
 *   base_table = "data_view_demo2",
 *   data_table = "my_data_view_demo2",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "question",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode"
 *   }
 * )
 */
class Data extends ContentEntityBase
{
	
	public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
		$fields['id'] = BaseFieldDefinition::create('integer')
			->setLabel(t('Poll ID'))
			->setDescription(t('The ID of the poll.'))
			->setReadOnly(TRUE)
			->setSetting('unsigned', TRUE);

		$fields['vid'] = BaseFieldDefinition::create('entity_reference')
			->setLabel(t('Author'))
			->setDescription(t('The poll author.'))
			->setSetting('target_type', 'user')
			->setTranslatable(TRUE)
			->setDefaultValueCallback('Drupal\poll\Entity\Poll::getCurrentUserId')
			->setDisplayOptions('form', array(
				'type' => 'entity_reference_autocomplete',
				'weight' => -10,
				'settings' => array(
					'match_operator' => 'CONTAINS',
					'size' => '60',
					'autocomplete_type' => 'tags',
					'placeholder' => '',
				),
			))
			->setDisplayConfigurable('form', TRUE);

		$fields['uuid'] = BaseFieldDefinition::create('uuid')
			->setLabel(t('UUID'))
			->setDescription(t('The poll UUID.'))
			->setReadOnly(TRUE);

		$fields['question'] = BaseFieldDefinition::create('string')
			->setLabel(t('Question'))
			->setDescription(t('The poll question.'))
			->setRequired(TRUE)
			->setTranslatable(TRUE)
			->setSetting('max_length', 255)
			->setDisplayOptions('form', array(
				'type' => 'string_textfield',
				'weight' => -100,
			));

		$fields['langcode'] = BaseFieldDefinition::create('language')
			->setLabel(t('Language code'))
			->setDescription(t('The poll language code.'));

    	return $fields;
    }
}