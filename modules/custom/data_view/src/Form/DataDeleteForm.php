<?php

/**
 * Generates a Drupal\data_view\Form\DataDeleteForm.
 */

namespace Drupal\data_view\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;

/**
 * 
 */
class DataDeleteForm extends ConfirmFormBase
{
	
	/**
	 * {inheritdoc}
	 */
	public function getFormId()
	{
		return 'data_delete_form';
	}

	public $cid;

	public function getQuestion() { 
		return t('Are you sure you want to delete this term?');
	}

	public function getCancelUrl() {
		return new Url('data_view.add');
	}

	public function getDescription() {
		return t('Only do this if you are sure!');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getConfirmText() {
		return t('Delete it!');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getCancelText() {
		return t('Cancel');
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state, $cid = NULL) {
		$this->id = $cid;
		$conn1 = Database::getConnection();
		$record1 = array();
		$query1 = $conn1->select('data_view_demo', 'd')
			->condition('term_name', $form_state->getValue('name'))
			->fields('d');
		$record1 = $query1->execute()->fetchAssoc();
		return parent::buildForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
		parent::validateForm($form, $form_state);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {
		$query = \Drupal::database();
		$query->delete('data_view_demo')->condition('id', $this->id)->execute();
		drupal_set_message('Data of <em>@cid</em> succesfully deleted!', ['@cid' => $this->cid]);
		$form_state->setRedirect('data_view.add');
	}
}