<?php

/**
 * Generates a Drupal\data_view\Form\DataAddForm.
 */

namespace Drupal\data_view\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * 
 */
class DataAddForm extends FormBase
{
	
	/**
	 * {inheritdoc}
	 */
	public function getFormId()
	{
		return 'data_add_form';
	}

	/**
	 * {inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state)
	{
		$form['name'] = [
			'#type' => 'textfield',
			'#title' => 'Enter News Type',
			'#required' => TRUE,
		];
		$form['actions']['#type'] = 'actions';
		$form['actions']['submit'] = [
			'#type' => 'submit',
			'#value' => $this->t('Submit'),
			'#button_type' => 'primary',
		];
		
		$header_of_table = array(
			'id'=>    t('Sr No'),
			'vid' => t('Taxonomy Term ID'),
			'term_name' => t('News Type'),
			'position' => t('Position'),
			'delete' => t('Operations'),
			// 'update' => t('Update'),
		);

		//select records from table
		$query = \Drupal::database()->select('data_view_demo', 'd');
		$query->fields('d', ['id','vid','term_name','position']);
		$results = $query->execute()->fetchAll();
		$rows=array();
		foreach($results as $data){
			$delete = Url::fromUserInput('/data_view/delete/'.$data->id);
			// $edit = Url::fromUserInput('/db_data/add?id='.$data->id);

			//print the data from table
			$rows[] = array(
				'id' =>$data->id,
				'vid' => $data->vid,
				'term_name' => $data->term_name,
				'position' => $data->position,
				
				\Drupal::l('Remove', $delete),
				// \Drupal::l('Edit', $edit),
			);
		}
		//display data in site
		$form['table'] = [
			'#type' => 'table',
			'#header' => $header_of_table,
			'#rows' => $rows,
			'#empty' => t('<b><i>No items found!</i></b>'),
		];
		return $form;
	}

	/**
	 * {inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state)
	{
		if (preg_match('/[^A-Za-z]/', $form_state->getValue('name'))) {
			$form_state->setErrorByName('name', $this->t('Value should be News Type Reference only!'));
		}
	}

	/**
	 * {inheritdoc}
	 */
	// public $position = 1;
	public function submitForm(array &$form, FormStateInterface $form_state)
	{
		$position = 1;
		try {
			$conn = Database::getConnection();
			$record = array();
			$query = $conn->select('taxonomy_term_field_data', 't')
				->condition('Name', $form_state->getValue('name'))
				->fields('t');
			$record = $query->execute()->fetchAssoc();

			if (!empty($record)) {
				$conn1 = Database::getConnection();
				$record1 = array();
				$query1 = $conn1->select('data_view_demo', 'd')
					->condition('term_name', $form_state->getValue('name'))
					->fields('d');
				$record1 = $query1->execute()->fetchAssoc();
				
				if (empty($record1)) {

					$fields = array(
						'vid' => $record['tid'],
						'term_name' => $form_state->getValue('name'),
						'position' => $position,
					);
					\Drupal::database()->insert('data_view_demo')->fields($fields)->execute();

					$position++;
					$form_state->setRedirect('data_view.add');
				} else {
					drupal_set_message($this->t('@error already exists!', ['@error' => $form_state->getValue('name')]));
				}
			} else {
				drupal_set_message($this->t('@error is not a News Type Reference!', ['@error' => $form_state->getValue('name')]));
			}
		} catch(\Exception $e) {
			drupal_set_message($this->t('@user', ['@user' => $e]));
		}
	}
}